<?php

namespace Autodoc\ApiBundle\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListNormalizer implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = [])
    {
        $violations = [];
        $messages = [];
        foreach ($object as $violation) {
            $propertyPath = $violation->getPropertyPath();
            $violationEntry = [
                'field' => $propertyPath,
                'title' => $violation->getMessage(),
            ];
            $violations[] = $violationEntry;
            $prefix = $propertyPath ? sprintf('%s: ', $propertyPath) : '';
            $messages[] = $prefix.$violation->getMessage();
        }

        $result = [];

        if (isset($context['status'])) {
            $result['status'] = $context['status'];
        }

        if (isset($context['instance'])) {
            $result['instance'] = $context['instance'];
        }

        $result['message'] = 'Validation failed';

        return $result + ['violations' => $violations];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ConstraintViolationListInterface;
    }
}
