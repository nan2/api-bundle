<?php

namespace Autodoc\ApiBundle\ArgumentResolver;

use Doctrine\Common\Annotations\AnnotationReader;
use Autodoc\ApiBundle\Exception\RequestValidationException;
use Autodoc\ApiBundle\Normalizer\ConstraintViolationListNormalizer;
use Autodoc\ApiBundle\Request\GetRequestInterface;
use Autodoc\ApiBundle\Request\PostRequestInterface;
use Autodoc\ApiBundle\Request\RequestDataInterface;
use Autodoc\ApiBundle\Request\RouteParamsRequestInterface;
use Autodoc\ApiBundle\Request\UserAgentAwareInterface;
use Autodoc\ApiBundle\Request\UserIpAwareInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorFromClassMetadata;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDataResolver implements ArgumentValueResolverInterface
{
    const PROPERTY_NORMALIZER = 'property';
    const OBJECT_NORMALIZER = 'object';

    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator, string $normalizer)
    {
        $this->validator = $validator;
        $normalizers = [];
        
        if ($normalizer === self::PROPERTY_NORMALIZER) {
            $normalizers[] = new PropertyNormalizer();
        }
        
        if ($normalizer === self::OBJECT_NORMALIZER) {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $discriminator = new ClassDiscriminatorFromClassMetadata($classMetadataFactory);
            
            $normalizers[] = new ArrayDenormalizer();
            $normalizers[] = new ObjectNormalizer(
                $classMetadataFactory,
                null,
                null,
                new PhpDocExtractor(),
                $discriminator
            );
        }

        $normalizers[] = new DateTimeNormalizer();
        $normalizers[] = new ConstraintViolationListNormalizer();
        $this->serializer = new Serializer($normalizers, [new JsonEncoder()]);
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_subclass_of($argument->getType(), RequestDataInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $data = [];
        if(is_subclass_of($argument->getType() ,  GetRequestInterface::class)){
            $requestData = $request->query->all();
            $data = array_merge($data, $requestData);
        }

        if(is_subclass_of($argument->getType() , PostRequestInterface::class)){
            if($request->getContentType() == 'json'){
                $requestData = $this->serializer->decode(
                    $request->getContent(),
                    'json'
                );
            }else{
                $requestData = $request->request->all();
            }
            $data = array_merge($data, $requestData);
        }

        if(is_subclass_of($argument->getType() , RouteParamsRequestInterface::class)){
            $requestData = $request->attributes->get('_route_params');
            $data = array_merge($data, $requestData);
        }

        $data = $this->serializer->denormalize($data, $argument->getType(), null, ['disable_type_enforcement' => true]);

        if ($data instanceof UserIpAwareInterface) {
            $data->setUserIp($request->getClientIp());
        }
        if ($data instanceof UserAgentAwareInterface) {
            $data->setUserAgent($request->headers->get('user_agent'));
        }

        $this->validate($data);

        yield $data;
    }

    private function validate(RequestDataInterface $data): void
    {
        $errors = $this->validator->validate($data);
        if (0 !== count($errors)) {
            $normalizedErrors = $this->serializer->normalize($errors, 'json');
            throw new RequestValidationException($normalizedErrors);
        }
    }
}
