Api Bundle
=

### **Installation**

1. Add git repository to composer.json
```
    "repositories": [
        { "type": "vcs", "url": "https://gitlab.autodoc.dev/pim/api-bundle.git" }
    ]
```

2. Require repository
```
    composer require pim/api-bundle
```