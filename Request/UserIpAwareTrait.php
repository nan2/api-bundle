<?php

namespace Autodoc\ApiBundle\Request;

trait UserIpAwareTrait
{
    public string $userIp;

    public function setUserIp(?string $userIp = null)
    {
        $this->userIp = $userIp;
    }
}
