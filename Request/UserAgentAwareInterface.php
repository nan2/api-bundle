<?php

namespace Autodoc\ApiBundle\Request;

interface UserAgentAwareInterface extends RequestDataInterface
{
    public function setUserAgent(?string $userAgent = null);
}