<?php

namespace Autodoc\ApiBundle\Request;

interface UserIpAwareInterface extends RequestDataInterface
{
    public function setUserIp(?string $userIp = null);
}