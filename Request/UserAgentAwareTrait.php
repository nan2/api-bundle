<?php

namespace Autodoc\ApiBundle\Request;

trait UserAgentAwareTrait
{
    public string $userAgent;

    public function setUserAgent(?string $userAgent = null)
    {
        $this->userAgent = $userAgent;
    }
}
