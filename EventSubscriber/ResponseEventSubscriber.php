<?php

namespace Autodoc\ApiBundle\EventSubscriber;

use Autodoc\ApiBundle\Response\ApiJsonResponseInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ResponseEventSubscriber implements EventSubscriberInterface
{
    private NormalizerInterface $normalizer;
    private string $traceHttpKey;
    private string $traceEnvKey;

    public function __construct(NormalizerInterface $normalizer, string $traceHttpKey, string $traceEnvKey)
    {
        $this->normalizer = $normalizer;
        $this->traceHttpKey = $traceHttpKey;
        $this->traceEnvKey = $traceEnvKey;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['resolveControllerResult', 200],
            ],
            KernelEvents::RESPONSE => [
                ['addTraceId', 1],
            ],
        ];
    }

    public function addTraceId(ResponseEvent $event): void
    {
        if ($event->isMainRequest()) {
            return;
        }

        if (!isset($_ENV[$this->traceEnvKey])) {
            return;
        }

        $event->getResponse()->headers->set(
            $this->traceHttpKey,
            $_ENV[$this->traceEnvKey]
        );
    }

    public function resolveControllerResult(ViewEvent $viewEvent): void
    {
        $controllerResult = $viewEvent->getControllerResult();

        if ($controllerResult instanceof ApiJsonResponseInterface) {
            $viewEvent->setResponse(
                new JsonResponse($this->normalizer->normalize($controllerResult))
            );
        }
    }
}