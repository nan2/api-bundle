<?php declare(strict_types=1);

namespace Autodoc\ApiBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RequestEventSubscriber implements EventSubscriberInterface
{
    private string $traceHttpKey;
    private string $traceEnvKey;

    public function __construct(
        string $traceHttpKey,
        string $traceEnvKey
    ) {
        $this->traceHttpKey = $traceHttpKey;
        $this->traceEnvKey = $traceEnvKey;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['setTraceId', 252]
            ],
        ];
    }

    public function setTraceId(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ($event->isMainRequest()) {
            return;
        }

        if (!$request->headers->has($this->traceHttpKey)) {
            $request->headers->set($this->traceHttpKey, md5(uniqid()));
        }

        $_ENV[$this->traceEnvKey] = $request->headers->get($this->traceHttpKey);
    }
}
