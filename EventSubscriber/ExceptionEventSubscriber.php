<?php

namespace Autodoc\ApiBundle\EventSubscriber;

use Autodoc\ApiBundle\Exception\RequestValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionEventSubscriber implements EventSubscriberInterface
{
    private string $errorNamespacePrefix;
    private array $exceptionList;

    public function __construct(array $exceptionList, int $errorNamespacePrefix)
    {
        $this->errorNamespacePrefix = $errorNamespacePrefix;
        $this->exceptionList = $exceptionList;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processClientException', 10],
            ],
        ];
    }

    public function processClientException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof RequestValidationException) {
            $event->setResponse(new JsonResponse($exception->getErrors(), 422));
        }

        $httpCode = $this->getExceptionHttpCode($exception);

        if ($httpCode) {
            $event->setResponse(new JsonResponse([
                'code' => $this->formatErrorCode($exception->getCode()),
                'message' => $exception->getMessage(),
            ], $httpCode));
        }
    }

    public function getExceptionHttpCode(\Throwable $exception): ?int
    {
        foreach ($this->exceptionList as $exceptionConfig) {
            if (in_array(get_class($exception), $exceptionConfig['exceptions'])) {
                return (int)$exceptionConfig['code'];
            }
        }

        return null;
    }

    private function formatErrorCode($errorCode): string
    {
        $errorNamespace = $this->errorNamespacePrefix * 1000;

        return $errorNamespace + $errorCode;
    }
}
