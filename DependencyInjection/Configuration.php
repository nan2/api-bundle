<?php

namespace Autodoc\ApiBundle\DependencyInjection;

use Autodoc\ApiBundle\ArgumentResolver\RequestDataResolver;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('api');
        $rootNode = $treeBuilder->getRootNode();

        $lastNode = $rootNode
            ->children()
                ->scalarNode('service_namespace')
                    ->isRequired()
                ->end();
        $lastNode = $lastNode
            ->arrayNode('json_error_responses')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('code')->end()
                            ->arrayNode('exceptions')
                                ->scalarPrototype()->end()
                            ->end()
                        ->end()
                    ->end()
            ->end();
        $lastNode = $lastNode->enumNode('normalizer')
                    ->values([RequestDataResolver::PROPERTY_NORMALIZER, RequestDataResolver::OBJECT_NORMALIZER])
                    ->defaultValue('property')
                ->end()
            ->end();

        $lastNode = $rootNode
            ->children()
                ->arrayNode('trace')
                    ->children()
                        ->scalarNode('http_key')->end()
                        ->scalarNode('env_key')->end()
                    ->end()
                ->end();


        return $treeBuilder;
    }
}