<?php

namespace Autodoc\ApiBundle\DependencyInjection;

use Autodoc\ApiBundle\ArgumentResolver\RequestDataResolver;
use Autodoc\ApiBundle\EventSubscriber\ResponseEventSubscriber;
use Autodoc\ApiBundle\Response\ErrorCodeProviderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ApiExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('nan.api.error_namespace_prefix', $config['service_namespace']);
        $container->setParameter('nan.api.exception_list', $config['json_error_responses']);
        $container->setParameter('nan.api.trace.http_key', $config['trace']['http_key']);
        $container->setParameter('nan.api.trace.env_key', $config['trace']['env_key']);

        $container
            ->autowire(RequestDataResolver::class, RequestDataResolver::class)
            ->setArgument(1, $config['normalizer'])
            ->addTag('controller.argument_value_resolver');

        $container
            ->autowire(ResponseEventSubscriber::class, ResponseEventSubscriber::class)
            ->setArgument(1, $config['trace']['http_key'])
            ->setArgument(2, $config['trace']['env_key'])
            ->addTag('kernel.event_subscriber');
    }
}
