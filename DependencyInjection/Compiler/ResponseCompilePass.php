<?php declare(strict_types = 1);

namespace Autodoc\ApiBundle\DependencyInjection\Compiler;

use Autodoc\ApiBundle\EventSubscriber\ResponseEventSubscriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ResponseCompilePass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        $container
            ->register(ResponseEventSubscriber::class, ResponseEventSubscriber::class);
    }
}
