<?php declare(strict_types = 1);

namespace Autodoc\ApiBundle\DependencyInjection\Compiler;

use Autodoc\ApiBundle\EventSubscriber\RequestEventSubscriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class RequestHandlerCompilePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $container
            ->register(RequestEventSubscriber::class, RequestEventSubscriber::class)
            ->addTag('kernel.event_subscriber')
            ->setArgument(0, $container->getParameter('nan.api.trace.http_key'))
            ->setArgument(1, $container->getParameter('nan.api.trace.env_key'));
    }
}
