<?php declare(strict_types = 1);

namespace Autodoc\ApiBundle\DependencyInjection\Compiler;

use Autodoc\ApiBundle\EventSubscriber\ExceptionEventSubscriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ExceptionHandlerCompilePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void 
    {
        $container
            ->register(ExceptionEventSubscriber::class, ExceptionEventSubscriber::class)
            ->addTag('kernel.event_subscriber')
            ->setArgument(0, '%nan.api.exception_list%')
            ->setArgument(1, '%nan.api.error_namespace_prefix%');
    }
}
