<?php declare(strict_types = 1);

namespace Autodoc\ApiBundle;

use Autodoc\ApiBundle\DependencyInjection\Compiler\ExceptionHandlerCompilePass;
use Autodoc\ApiBundle\DependencyInjection\Compiler\RequestHandlerCompilePass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApiBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new ExceptionHandlerCompilePass());
        $container->addCompilerPass(new RequestHandlerCompilePass());
    }
}
